/* 	Name: Arzoo Karki
	CSE 002-310
	Professor Arielle Karr
	11/13/2018
	Program to practice searching single dimensional arrays
*/

import java.util.Scanner; 
public class CSE2Linear {	
public static void main(String[] args) { 

Scanner myOwnScanner = new Scanner(System.in);

int[] input = new int[15];

int k = 0;
int iterations = 0;

System.out.println ("Enter 15 ascending ints for final grades in CSE2:");

for (int i = 0; i < input.length; ++i) {

input[i] = ABC(myOwnScanner,i);
	
if (i != 0 && input[i] < input[i-1]) {
	
System.out.println("Please enter the values in an ascending order: ");
i--; 

continue;
}
}

PrintArray (input); 
System.out.print ("Enter a grade to search for: ");

k = myOwnScanner.nextInt();
iterations = BinarySearch(input,k);

if (iterations != -1) {
	
System.out.println (k + " was found in the list with " + iterations + " iterations");

}

input = ScramblingMethod(input);//scrambling grades

System.out.println("Scrambled:");

PrintArray (input); //printing scrambled grades

System.out.print("Enter a grade to search for: ");

k = myOwnScanner.nextInt();
iterations = LinearSearching(input,k);

if (iterations != -1) {
System.out.println (k + " was found in the list with " + iterations + " iterations");
}
	}

public static int ABC(Scanner scan, int i) {

int r;

System.out.print (""+(i+1)+" th value of the grid: ");

while (!scan.hasNextInt()) { //checking if the input was an integer 
	
    System.out.println("Please enter an integer."); 
    String asd = scan.next();
    System.out.print(""+(i+1)+" th value of the grid: ");
}
	
r = scan.nextInt();
while (!(r >= 0 && r <= 100)){
	
System.out.println("Please enter an integer in a specified range.");
System.out.print(""+(i+1)+" th value of the grid: ");

	while(!scan.hasNextInt()){ //checking if the input from user was an integer or not
	
	System.out.println("Please enter an integer.");
	String junkWord = scan.next();
	System.out.print(""+(i+1)+" th value of the grid: ");
	}

	r = scan.nextInt(); // the input that has passed all tests
}

return r; 

}

public static void PrintArray(int[] list) {
	
for ( int i = 0; i < list.length; ++i) {
System.out.print (list [i] + " " );
}

System.out.println();

}

public static int[] ScramblingMethod(int[] list) { 

for (int e = 0; e < 51; e++) { //shuffling the array 

	for (int i = 0; i < list.length; ++i) {
	int j = (int)(( Math.random () * (14 - i) ) + 1) ; 
	
	int tempValue = list[j]; //replacing 
	list[j] = list[i]; //replacing
	list[i] = tempValue; //replacing
	}
  }

  return list;
}

	// Professor Karr's code
	// from Course site

public static int BinarySearch(int[] list, int key){//method for the binary search
int low = 0;//declaring and initializing the lowest index
int high = list.length-1;//declaring and initializing the highes index
int count = 0;//declaring and initializing the the number of iterations
while(high >= low) {
count++;//increment in the iteration
int mid = (low + high)/2;//the middle index point
if (key < list[mid]) {
high = mid - 1;
}
else if (key == list[mid]) {
return count;//returning the number of iterations
}
else {
low = mid + 1;
}
}
	System.out.println (key + " was not found in the list with " + count + " iterations");//printing that the key wasn't found
return -1;
}
	
  
public static int LinearSearching(int[] list, int key){
	//method for searching with linear method
	
int count = 0;
	//declaring and initializing the number of iterations

for ( int i = 0; i < list.length; ++i) {
count++;

if(list[i] == key) {
return count;
}
}

System.out.println(key+" was not found in the list with " + count + " iterations");
return -1;

}
}