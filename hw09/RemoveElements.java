/* 	Name: Arzoo Karki
CSE 002-310
Professor Arielle Karr
11/13/2018
Program to practice searching single dimensional arrays
*/

import java.util.Scanner;
public class RemoveElements {
	
  public static void main(String [] args) {
	  
	Scanner scan = new Scanner(System.in);
	
	int num [ ] = new int [10];
	int newArray1 [];
	int newArray2 [];
	int index, target;
	String answer = "" ;
	
	do {
	  System.out.println ("Random input 10 ints [0-9]");
	  num = randomInput();//printing the random elements in an array
	  String out = "The original array is:";
	  out += listArray(num);
	  System.out.println(out);
	 
	  	System.out.print("Enter the index: ");
	  	index = scan.nextInt();
	  	newArray1 = delete(num,index);
	  	String out1="The output array is: ";
	  	out1+=listArray(newArray1); 
	  	System.out.println(out1);
	 
	    System.out.print("Enter the target value: ");
	  	target = scan.nextInt();
	  	newArray2 = remove(num,target);
	  	String out2="The output array is: ";
	  	out2+=listArray(newArray2); 
	  	System.out.println(out2);
	  	 
	  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit: ");
	  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]) {
	String out="{";
	for(int j=0;j<num.length;j++) {
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
  
  public static int[] randomInput(){
	int[] randomArray = new int[10];
  	for(int i=0;i<randomArray.length;++i){
  		randomArray[i] = (int)(Math.random()*(10));
  	}
  	return randomArray;
  }
  
  public static int[] delete(int[] list, int pos){
  	if(pos >= 10){
  		System.out.println("The index is not valid.");
  		return list;
  	}
  	int[] test = new int[list.length - 1];
  	for (int i = 0; i < pos; ++i) {
  		test[i] = list[i];
  	}
  	
  	for (int i = pos; i < test.length; ++i) {
  		test[i] = list[i+1];
  	}
  	return test;
  }
  
  public static int[] remove(int[] list, int target) {
  	int x = 0; 
  	int y = 0;
  	for ( int i = 0; i < list.length; ++i) { 
  		if ( target == list[i]) {
  			x++;
  		}
  	}
  	
  	if(x == 0) {
  		System.out.println ("Element " + target + " was not found.");
  		return list;
  	}
  	System.out.println("Element " + target + " has been found.");
  	
  	int[] test = new int[list.length-x];
  	
  	for(int i=0;i<list.length;++i) {
  		if(target == list[i]) {
  			y++; 
  			continue;
  		}
  		test[i-y] = list[i];
  	}
  	return test;
  	
  }
}

