/* 
Name: Arzoo Karki
CSE 002-310
Professor Arielle Karr
10/4/2018
Program to practice loops and use myScanner.hasNext()
*/

import java.util.Scanner; // importing the Scanner class to use it

public class userInput { // declaring class as userInput
public static void main(String args[]){ //initializing main method
Scanner myOwnScanner = new Scanner (System.in); // declaring a myOwnScannerner object

// initializing a bunch of string and integer variables to be used throughout this program
int numCourse;
int numInWeek;
int numStudents;
String timeOfClass = "";
int checkTime;
String nameDept = "";
String nameInstructor = "";
String invalidInputs = ""; 
  
System.out.print ("Enter your course number: "); // prompting user to input course number
 
while (!myOwnScanner.hasNextInt()){ // making sure the input is an integer
  System.out.print ("Your input is invalid. Enter your course number: "); // prompting user to input course number
  invalidInputs = myOwnScanner.next(); // pushing all invalid inputs for only valid ones to be used
  }
  
numCourse = myOwnScanner.nextInt(); // receiving the valid integer course number
        
// in every section below, the code prompts the user to input the required data in the required format
  // it checks if the input is in the required format
  // if not, it asks the user to input again, until it is in valid format
  // finally, it prints every variable
 System.out.print("Enter your department name: ");
 
 while (!myOwnScanner.hasNext() || myOwnScanner.hasNextInt() || myOwnScanner.hasNextDouble() || myOwnScanner.hasNextFloat()){
  System.out.print ("Your input is not valid. Enter your department name: ");
  invalidInputs = myOwnScanner.next();
   }
  
  nameDept = myOwnScanner.next();
        
  System.out.print("Enter the number of times your class meets in a week: ");
        
  while (!myOwnScanner.hasNextInt()){
    System.out.print ("Your input is not valid. Enter the integer number of times: ");
    invalidInputs = myOwnScanner.next();
     }
     
  numInWeek = myOwnScanner.nextInt();
        
  System.out.print("Enter the time at which your class meets in format - 0000-2359: ");
  
  while (!myOwnScanner.hasNextInt()){
     System.out.print ("Your input is not valid. Enter the time in format between 0000-2359: ");
     invalidInputs = myOwnScanner.next();
        }
  
        while (myOwnScanner.hasNextInt()){
          
        checkTime = myOwnScanner.nextInt();
        if (checkTime < 0 || checkTime > 2359) {
        System.out.print ("Your format is not valid. Input time in military format 0000-2359: ");
        invalidInputs = Integer.toString(checkTime);
        }
          
        else if (checkTime >= 0 && checkTime <= 2359){
            
          if (checkTime >= 100){
            int timeMin = checkTime % ( (checkTime/100) *100); //initializing timeMin 
                
            if (timeMin >= 0 && timeMin <= 59){ // checking hat input has its last two digits between 00 and 59
                    break;
                }
                
            else{
             System.out.print ("Your time is not in valid format. Enter 0000 through 2359.");
             invalidInputs = Integer.toString(checkTime);
                }
            }
          
        else if (checkTime < 100) {
            int timeMin = checkTime;
            
          if (timeMin >= 0 && timeMin <= 59){ 
                    break;
                }
                
          else {
             System.out.print ("Your time is not in valid format. Enter 0000 through 2359.");
            invalidInputs = Integer.toString(checkTime);
                }
            }
        }
        }
        
System.out.print("Enter the name of your instructor: ");
        
while (myOwnScanner.hasNextInt() || myOwnScanner.hasNextDouble() || myOwnScanner.hasNextFloat() || !myOwnScanner.hasNext()){
   System.out.print ("Your input is not valid. Enter your instructor's name: ");
   invalidInputs = myOwnScanner.next();
        }
  
nameInstructor = myOwnScanner.next();
        
System.out.print("Enter the number of students in your class: ");

while (!myOwnScanner.hasNextInt()){
    System.out.print ("Your input is not valid. Enter the integer number of students: ");
    invalidInputs = myOwnScanner.next();
        }
 
 numStudents = myOwnScanner.nextInt();
        
 // printing out required outputs
 System.out.println("Your course number is: " + numCourse);
 System.out.println("Your department name is: " + nameDept);
 System.out.println("You meet " + numInWeek + " times in a week.");
 System.out.println("Your class meets at:" + timeOfClass);
 System.out.println("Your course instructor is: " + nameInstructor);
 System.out.println("You have " + numStudents + " students in your class.");
        
    }
    
}

          
        

    
      
    