/* 
Name: Arzoo Karki
CSE 002-310
Professor Arielle Karr
11/13/2018
Program to practice manipulation of arrays
*/

import java.util.Scanner; //import Scanner class
public class hw08 { 
  
	public static void main(String[] args) { 
		Scanner scan = new Scanner(System.in); 
		//suits club, heart, spade or diamond 
    
		String[] suitNames={"C","H","S","D"};   
		String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"};
		String[] cards = new String[52];
		String[] hand = new String[5];
		int numCards = 7;
		int again = 1;
		int index = 51;
		for (int i=0; i<52; i++) {
		  cards[i] = rankNames[i%13] + suitNames[i/13]; 
		} 
    
		System.out.println();
		printArray(cards);
		cards = shuffle(cards);
		System.out.println("Shuffled");
		printArray(cards);
		while(again == 1){
		if(numCards > index + 1){
			for (int i=0; i<52; i++){ 
		  		cards[i]=rankNames[i%13]+suitNames[i/13]; 
		  	}
		  	cards = shuffle(cards);
		  	index = 51;
		} 
      
		hand = getHand (cards,index,numCards); //getting cards in a hand
		System.out.println ("Hand"); 
		printArray(hand); //printing out the hand
		index = index - numCards; //change in index
		System.out.println("Enter a 1 if you want another hand drawn");  
		again = scan.nextInt(); //taking in user response
		
    }  
  	}
  
  public static String[] getHand ( String [] list, int index, int numCards){ 
  System.out.print ("Number of desired cards: ");  
  Scanner scanner = new Scanner (System.in);    
  numCards = scanner.nextInt();
  
  String [] desiredLength = new String[numCards];   
  for ( int i =0; i < desiredLength.length; i++){  // picking the cards from the entire deck to enter into the hand  

    desiredLength[i] = list[index] ;
    index = index - 1;  
  }
    
  return desiredLength; 
  // here, we are returing the desired length to the main method
}
  
  	public static void printArray (String[] list) {
  		for (int i = 0; i < list.length; ++i) { // here, we are printing all the elements contained in the array
  			System.out.print (list[i] + " ");
  		}
      
  		System.out.println ();
  	}
  
  	public static String[] shuffle (String[] list) {
  		//here, we are shuffling the deck of cards for 51 times
      
  		for (int i = 0; i <= 50; i++){
	  		for (int j = 0; j < list.length; ++j){
	  			
	  			int n = (int) ((Math.random() * ( 51 - j ) ) + 1 ); 
          // here, we are generating a random index from where we can shuffle
	  			
          String value = list[j];
	  			list[n] = list[j];
	  			list[j] = value;
	  		}
  		}
      
  		return list; 
      // here, we are returing the shuffled deck to the main method
  	} //ending of the shuffle method
  
  
   //ending of the get hand method 

}
