import java.util.Scanner; //importing the Scanner
public class hw07 { //declaring class as hw07

		
public static String sampleText() {
	Scanner myOwnScanner = new Scanner(System.in); // creating scanner for inputs
	System.out.println ("Enter a sample text: "); // prompting the user to input sample text
	String userInput = myOwnScanner.nextLine(); // getting the user's sample text
	System.out.println("You entered: " + userInput); // printing out the required sample text
	
	return userInput; // returning the userinput
	}

public static void main(String args[]) { // main method
String userInput = sampleText(); 
char userOption = printMenu(); // storing the option selected by user for further evaluation of sample text

while (userOption!= 'q') { // as long as the user does not want to quit
                           // the following operations take place
  
	// this prompts the user to input an option from the list
  // if not, it keeps redirecting the user to the list of valid options
  while (!((userOption == 'c') || (userOption == 'w') || (userOption == 'f') || (userOption == 'r') || (userOption == 's') || (userOption == 'q'))){
		userOption = printMenu();
		}
	
  // in the following lines, in case of a valid option entered by the user
  // respective operations happen
  // required outputs are printed to the screen
    if (userOption == 'c') {
		System.out.println("Number of non-whitespace characters: " + getNumOfNonWSCharacters(userInput));
	}
	else if (userOption == 'w') {
		System.out.println(("Number of words: " + getNumOfWords(userInput)));
	}
	else if (userOption == 'f') {
		Scanner myOwnScanner = new Scanner(System.in);
		System.out.println ("Enter a word or phrase to be found: ");
		String userInput2 = myOwnScanner.nextLine();
		System.out.println ( "\"" + userInput2 + "\"" + " " + "instances: " + findText(userInput2, userInput));
	}
	else if (userOption == 'r') {
		System.out.println ("Edited text: " + replaceExclamation(userInput));
	}
	else if (userOption == 's') {
		System.out.println ("Edited text: " + shortenSpace(userInput));
	}
	userOption = printMenu();
}
}

public static char printMenu() { //declaring method
	Scanner myOwnScanner = new Scanner(System.in);
	System.out.println ("MENU\r\n" + // prompting user towards list of valid options 
			"c - Number of non-whitespace characters\r\n" + 
			"w - Number of words\r\n" + 
			"f - Find text\r\n" + 
			"r - Replace all !'s\r\n" + 
			"s - Shorten spaces\r\n" + 
			"q - Quit\r\n" + 
			" \r\n" + 
			"Choose an option:\r\n" + 
			"");
	String userInput = myOwnScanner.next(); // taking in the option from the user
  
	// evaluating the first character only
  // in case more than one character is entered
  char userOption = userInput.charAt(0);  
	
	return userOption;
	}

public static int getNumOfNonWSCharacters (String userOption) { //declaring method
	int count = 0;
	for (int i = 0; i < userOption.length(); i++) { // running index through the length of input
	  
    // if charater at that index is not a white space
    // counter is incremented
    if (!(Character.isWhitespace(userOption.charAt(i)))) count++; 
	}
	return count; // returning count
}

public static int getNumOfWords( String userOption) { //declaring method
	int wordCount = 0;

    boolean word = false;
    int endOfLine = userOption.length() - 1; // length of the user's input

    for (int i = 0; i < userOption.length(); i++) {
        // if the char is a letter, word = true.
        if (Character.isLetter(userOption.charAt(i)) && i != endOfLine) {
            word = true;
            // if char isn't a letter and there have been letters before,
            // counter goes up.
        } else if (!Character.isLetter(userOption.charAt(i)) && word) {
            wordCount++;
            word = false;
            // last word of String; if it doesn't end with a non letter, it
            // wouldn't count without this.
        } else if (Character.isLetter(userOption.charAt(i)) && i == endOfLine) {
            wordCount++;
        }
    }
    return wordCount; // returning wordCount
}	

public static int findText (String testWord, String userInput) { //declaring method 
       int count=0;
       String []s1=userInput.split(" "); //splitting the string for evaluation
       

       for(int i=0;i<s1.length;i++) // running through entire length of string
       {
          if(s1[i].equals(testWord)) // checking if the test word has been matched
           {
               count++; // incrementing counter
           }
       }
       return count; // returning count
   }

public static String replaceExclamation (String userInput) { //declaring method    
	int count = userInput.length();
	String testString = "";
	int j = 0;
	for ( int i = 0; i < count; i++ ) { // running through entire length of string
		if (userInput.charAt(i) == '!') { // checking if the character at any point is a !
			testString += userInput.substring(j, i) + "." ; // replacing the spotted ! with a .
			j = i + 1; // this continues to check for another ! from the last replaced position
		}
			}
	testString += userInput.substring(j); // the edited part of the text is added and the code is continued
	return testString; // returning test string
}
public static String shortenSpace (String userInput) { //declaring method    
	int count = userInput.length(); // running through entire length of string
	String testString = "";
	int j = 1;
	for ( int i = 0; i < count - 1; i++ ) {
		if (userInput.charAt(i) == ' ') { // checking if the character at any point is a space
			
      // here, the part of the input before multiple spaces is added to test string
      // one space is also inserted
      // the edited part of the input is saved
      
      testString += userInput.substring(j-1, i) + " " ; 
			
      int k = i + 1;
			
      // after a space has been detected
      // all counters increase
      // the operations continue
      while (userInput.charAt(k) == ' ') {
				j = j + 1;
				k = k + 1;
				i = i + 1;
			}
			i = k;
			j = i + 1;
		}
		
			}
	testString += userInput.substring ( j - 1 ); // adding the edited part of input to testString
	return testString; // returning testString

} // termination	
} // termination 
