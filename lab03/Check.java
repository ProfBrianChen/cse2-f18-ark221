
import java.util.Scanner; // importing the Scanner class to use it

public class Check{ // declaring the class 
  public static void main(String args[]){

    double costPerPerson; // declaring the variable to assign the cost per person
    double totalCost; // declaring the variable to assign the total cost to be paid
    int dollars; // declaring the varibale to store cost per person as an integer
    int dimes; // declaring the cost in the tenth place
    int pennies; // declaring the cost in the ones place
    
Scanner myOwnScanner = new Scanner (System.in); // declaring a scanner object 
    System.out.print ("Enter the original cost of the check in the form xx.xx: "); // prompting the user to enter the total cost of the check
    double checkCost = myOwnScanner.nextDouble(); // declaring a statement to accept the user's input
    System.out.print ("Enter the percentage tip that you wish to pay as a whole number in the form xx: "); // prompting the user to input the percentage tip they wish to pay
    double tipPercent = myOwnScanner.nextDouble(); // declaring a statement to accept the user's input
    tipPercent /= 100; // converting tip percent into a decimal valie
    System.out.print ("Enter the number of people who were at the dinner: "); // prompting the user to input the number of people among whom cost has to be split
    int numPeople = myOwnScanner.nextInt(); // declaring a statement to accept the user's input
    
    totalCost = checkCost * ( 1 + tipPercent); // adding the cost on the check and the tip amount
    costPerPerson = totalCost / numPeople; // dividing the total sum to be paid among the guests
    dollars = (int) costPerPerson; // obtaining the part of the cost only before decimals
    dimes = (int) (costPerPerson * 10) % 10; // obtaining the dimes amount
    pennies = (int) (costPerPerson * 100) % 10; // obtaining the  pennies amount
    System.out.println ("Each person in the group has to pay $" + dollars + "." + dimes + pennies); // printing out the cost each person has to pay with a meaningful output statement
        
  }
} // termination