/* 
Name: Arzoo Karki
CSE 002-310
Professor Arielle Karr
11/14/2018
Program to generate one dimensional array
*/


import java.util.Random; 
public class lab08 { 
public static void main (String [] args ) { 
  
Random randNum = new Random (); 
  
int [] array1 = new int [100];

int [] array2 = new int [100];
  
int n = 0;
int i = 0;

for ( i = 0; i < array1.length; i++) { 
  array1 [i] = randNum.nextInt (100);
}

for ( i = 0; i < array1.length; i++ ) {
  n = array1 [i];
  array2[n]++; 
} 
  
for ( i =1; i < array2.length; i++ ) { 
  if (array2 [i] >= 1) {
    
  System.out.println(i + " occurs " + array2[i] + " times"); 
    
  }
}
}
}
