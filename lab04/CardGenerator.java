public class CardGenerator { // declaring class as Card Generator
  public static void main(String args[]){
    
    String nameSuit = ""; //initializing the variable name to store name of suit
    String cardIdentity; //initializing the variable name to store identity of card
    
    int randNum = (int) (Math.random () *( 52 + 1 )) + 1; // generating a random number to be evaluated
    int remNum = (int)(randNum % 13); //generating a remainder of the random number to determine card identity
    
     if ((randNum >= 1) && (randNum <= 13)) { //declaring the condition
      nameSuit = "Diamonds";} //assigning the variable with an appropriate name suit 
    
     else if ((randNum >= 14) && (randNum <= 26)) { //declaring the condition
      nameSuit = "Clubs"; } //assigning the variable with an appropriate name suit 
    
     else if ((randNum >= 27) && (randNum <= 39)) { //declaring the condition
      nameSuit = "Hearts"; } //assigning the variable with an appropriate name suit 
    
     else if ((randNum >= 40) && (randNum <= 52)) { //declaring the condition
      nameSuit = "Spades"; } //assigning the variable with an appropriate name suit 
    
    switch (remNum) { //initializing switch statement
        //declaring respective cases and assigning appropriate values to card identity according to the case
      case 0: 
        cardIdentity = "King"; 
        break; //breaking eash case
      case 12: 
        cardIdentity = "Queen";
        break;
      case 11: 
        cardIdentity = "Jack";
        break;
      case 10: 
        cardIdentity = "10";
        break;
      case 9: 
        cardIdentity = "9";
        break;
      case 8: 
        cardIdentity = "8";
        break;
      case 7: 
        cardIdentity = "7";
        break;
      case 6: 
        cardIdentity = "6";
        break;
      case 5: 
        cardIdentity = "5";
        break;
      case 4: 
        cardIdentity = "4";
        break;
      case 3: 
        cardIdentity = "3";
        break;
      case 2: 
        cardIdentity = "2";
        break;
      case 1: 
        cardIdentity = "Ace";
        break;
      default: 
        cardIdentity = "Invalid"; 
        break;
		}

     System.out.println ("You picked the " + cardIdentity + " of " + nameSuit); //printing out the required output
    
  }
  }
    