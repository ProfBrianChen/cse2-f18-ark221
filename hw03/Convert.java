import java.util.Scanner; // importing the Scanner

public class Convert{ // declaring class as Convert
  public static void main(String args[]){
    
    final double ACRESTOSQMILES = 0.0015625; // declaring constant values for conversion 
    final double INCHESTOMILES = 1.57828e-5; // declaring constant values for conversion 
    double areaInSqMiles; // declaring variable to assign the area in miles
    double rainInMiles; // declaring variable to assign the rain in miles
    double cubicMilesRain; // declaring variable to assign the rain in cubic miles
    
    Scanner myScanner = new Scanner (System.in); 
    
    System.out.print ("Input the number of acres of land affected by hurricane precipitation in the form xx.xx: "); // prompting user for input
    double numAcresLand = myScanner.nextDouble(); // getting input from the user
    System.out.print ("Input the inches of rain that were dropped on average in the form xx.xx: "); // prompting user for input
    double inchesRain = myScanner.nextDouble(); // getting input from the user
    
    areaInSqMiles = numAcresLand * ACRESTOSQMILES; // converting the area in acres to square miles
    rainInMiles = inchesRain * INCHESTOMILES; // converting the rain in inches to miles
    cubicMilesRain = areaInSqMiles * rainInMiles; // multiplying area in square miles and rain in miles to get rain in cubic miles
    
    System.out.println ("Quantity of rain in cubic miles is: " + cubicMilesRain); // printing out the rain in cubic miles
  }
}
    
