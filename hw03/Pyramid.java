import java.util.Scanner; // importing the Scanner

public class Pyramid{ // declaring class as Pyramid
  public static void main (String args [] ) {
    
    double volumePyramid; // declaring variable to assign the volume of pyramid
    
    Scanner myScanner = new Scanner (System.in);
    
    System.out.print ("The square side of the pyramid in the form xx.xx is: "); // prompting user for input
    double squareSide = myScanner.nextDouble(); // getting input from the user
    
    System.out.print ("The height of the pyramid in the form xx.xx is: "); // prompting user for input
    double heightPyramid = myScanner.nextDouble(); // getting input from the user
    
    volumePyramid = ( Math.pow(squareSide,2) * heightPyramid ) / 3; // calculating the volume using given dimensions
    
    System.out.println ("The volume inside the pyramid is: " + volumePyramid); // printing out the volume inside a pyramid
    
  }
}
    
