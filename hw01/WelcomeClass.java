public class WelcomeClass { // initializing program execution
    public static void main (String [] args) {
      
System.out.println("  -----------"); // printing the first line of the required output keeping indents in mind
System.out.println("  | WELCOME | "); // printing the second line using string literals
System.out.println("  -----------"); // printing the third line
System.out.println("  ^  ^  ^  ^  ^  ^"); // printing the fourth line with appropriate spaces
System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\"); // to print a single backslash, the string literal needs to have double backslash 
System.out.println("<-A--R--K--2--2--1->"); // printing out mu Lehigh username
System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /"); // printing out the backslashes accprding to the Java rules 
System.out.println("  v  v  v  v  v  v "); // printing out the last line
                   
    }
 } //terminating the program


