import java.util.Scanner; //importing the Scanner
public class EncryptedX { //declaring class as EncryptedX

  public static void main (String args[]) {
    Scanner myOwnScanner = new Scanner(System.in);
    
	boolean correct = false; //declaring boolean for input validation similar to the one in lab06
	String invalidInputs = ""; //declaring string tu push invalid inputs of the user
	int userInput = 0; //initializing userInput
    
	System.out.print ("Enter an integer between 0 and 100: "); //prompting the user for a valid input
	while (!correct) { // checking condition
    
	  while (!myOwnScanner.hasNextInt()){ //making sure the input is an integer
        System.out.print ("Your input is invalid. Enter an integer between 0 and 100: "); //prompting user to input an integer
	    invalidInputs = myOwnScanner.next(); //pushing all invalid inputs for only valid ones to be used
	  }
      userInput = myOwnScanner.nextInt(); //making sure input is in desired range
      if (userInput >= 0 && userInput <= 100) { 
	    correct = true;
	  }
	  else {
	    System.out.print ("Your input is out of range. Enter an integer between 0 and 100: "); //prompting user to input the integer in a desired range
		correct = false;
      }
    } 
    //input validation ends here

	//nested loop begins
    for (int i = 0; i <= userInput; i++) { //outer loop to deal with columns
	  for (int j = 0; j <= userInput; j++) { //inner loop to deal with rows
	    if (j == i) { //specifying when we want spaces to be printed out
		  System.out.print(" "); //printing out spaces
		}
		else if ( j == userInput - i) { //specifying another location for spaces
		  System.out.print(" "); //printing out spaces
		}
		else  {
		  System.out.print("*"); //filling in the rest of the location with * to obtain the encrypted X
		}
	  }
	  System.out.println(); //pushing output to a new line after every 'for' loop is complete
	}
  } //terminating method
} //terminating class
		
		
		
	