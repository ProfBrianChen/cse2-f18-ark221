/* 
Name: Arzoo Karki
CSE 002-310
Professor Arielle Karr
10/31/2018
Program to generate some basic random sentences
*/


import java.util.Random; // importing the Random class
import java.util.Scanner; // importing the Scanner class 
public class lab07 {  // declaring class 
public static void main (String [] args){  // declaring main method
  
  Random randGen = new Random(); // creaing the Random object
  int randomInt = randGen.nextInt (10) +1; // using Random class to generate a random integer
  
  genAdjective (randomInt);
  System.out.print ("The" + genAdjective(randomInt) );
  
  randomInt = randGen.nextInt (10) + 1; 
  System.out.print (genAdjective (randomInt));
  
  System.out.print (genNoun (randomInt) ); 
  System.out.print (genVerb ( randomInt) + " the");
  
  randomInt = randGen.nextInt (10) + 1;   
  System.out.print (genAdjective (randomInt));
  
  randomInt = randGen.nextInt (10)+ 1;
  System.out.print(genNoun (randomInt));  
   
  System.out.println ("\n"); 
  for ( int i = 0; i <= 5; i++) {
      randomInt = randGen.nextInt (10) +1; 
  
    genAdjective (randomInt);
     
  System.out.print ("The" + genAdjective (randomInt));
  randomInt = randGen.nextInt(10)+ 1; 
  System.out.print (genAdjective (randomInt));
  System.out.print(genNoun (randomInt)); 
  System.out.print (genVerb (randomInt) + " the");
   
  randomInt = randGen.nextInt(10) + 1;   
  System.out.print (genAdjective (randomInt));
  randomInt = randGen.nextInt (10)+ 1;
  System.out.print (genNoun (randomInt));  
  System.out.println ();
   }
}
  
  
public static String genAdjective (int randomInt) {
String adjective = "";
  
 switch (randomInt) { 
   case 1: 
    return " pretty"; 
   
   case 2: 
     return " petty"; 
   
   case 3: 
     return " caring"; 
   
   case 4: 
      return " lovable"; 
    
   case 5: 
        return " angry"; 
      
   case 6: 
       return " delightful"; 
  
   case 7: 
        return " eager"; 
      
   case 8: 
    return " beautiful"; 
      
   case 9: 
        return " faithful"; 
      
   case 10: 
      return " trustful"; 
      
   default: 
   
    return  "Invalid pick"; 
   
 }
}
  
 public static String genNoun(int randomInt){
String adjective = "";
   
 switch(randomInt){ 
   case 1: 
    return" dog"; 
   
   case 2: 
     return " tree"; 
   
   case 3: 
     return " computer"; 
   
   case 4: 
      return " table"; 
    
     case 5: 
        return " chair"; 
      
     case 6: 
       return " umbrella"; 
  
     case 7: 
        return " car"; 
      
     case 8: 
    return " bar"; 
      
     case 9: 
        return " cat"; 
      
     case 10: 
      return " mat"; 
      
   default: 
     return  "Invalid pick"; }
 
}

public static String genVerb(int randomInt){
String adjective = "";
 
 switch(randomInt){ 
   case 1: 
    return" drop"; 
   
   case 2: 
     return " drive"; 
   
   case 3: 
     return " drink"; 
   
   case 4: 
      return " dress"; 
    
     case 5: 
        return " dream"; 
      
     case 6: 
       return " draw"; 
  
     case 7: 
        return " beat"; 
      
     case 8: 
    return " become"; 
      
     case 9: 
        return " begin"; 
      
     case 10: 
      return " bend";   
   default: 
   return  "Invalid pick"; 
     
     
 }}}