public class Arithmetic{ // declaring class as Arithmetic
  public static void main(String args[]){
    
    int numPants; //declaring the number of pants bought
    int numShirts; //declaring the number of shirts bought
    int numBelts; //declaring the number of belts bought
    double costPants; // declaring the cost of a single pair of pants
    double costShirt; // declaring the cost of a shirt
    double costBelt; // declaring the cost of a single belt
    double taxRate; // declaring the rate of tax in the state of PA
    double totalcostPants; // declaring the total cost of all pants bought
    double totalcostShirts; // declaring the total cost of all shirts bought
    double totalcostBelts; // declaring the total cost of all belts bought
    double salestaxPants; // declaring the sales tax on pants bought
    double salestaxShirts; // declaring the sales tax on shirts bought
    double salestaxBelts; // declaring the sales tax on belts bought
    double totalcostItemsOnly; // declaring the total cost of pants, shirts, and belts without sales tax
    double totalSalesTax; // declaring the total sales tax on pants, shirts, and belts bought
    double totalcostWithTax; // declaring the grand total amount which includes the sales tax as well
        
    numPants = 3; // assigning the number of pants bought to the variable
    numShirts = 2; // assigning the number of shirts bought to the variable
    numBelts = 1; // assigning the number of elts bought to the variable
    costPants = 34.98; // assigning the cost of a single pair of pants to the variable
    costShirt = 24.99; // assigning the cost of a shirt to the variable
    costBelt = 33.99; // assigning the cost of a belt to the variable
    taxRate = 0.06; // assigning the tax rate in PA to the variable
        
    totalcostPants = numPants * costPants; // calculating the total cost of all the pants bought
    totalcostShirts = numShirts * costShirt; // calculating the total cost of all the shirts bought
    totalcostBelts = numBelts * costBelt; // calculating the total cost of all the belts bought
    salestaxPants = totalcostPants * taxRate; // calculating the sales tax to be paid on pants only
    salestaxShirts = totalcostShirts * taxRate; // calculating the sales tax to be paid on shirts only
    salestaxBelts = totalcostBelts * taxRate; // calculating the sales tax to be paid on belts only
    totalcostItemsOnly = totalcostPants + totalcostShirts + totalcostBelts; // calculating the total cost of pants, belts, and shirts without sales tax
    totalSalesTax = salestaxPants + salestaxShirts + salestaxBelts; // calculating the total sales tax to be paid on pants, belts, and shirts
    totalcostWithTax = totalcostItemsOnly + totalSalesTax; // calculating the grand total sum of items with their sales tax
            
    System.out.printf ("\n The total cost of pants bought is $" + "%.2f" , totalcostPants); // printing out the total cost of pants bought
    System.out.printf ("\n The total cost of shirts bought is $" + "%.2f" , totalcostShirts); // printing out the total cost of shirts bought
    System.out.printf ("\n The total cost of belts bought is $" + "%.2f" , totalcostBelts); // printing out the total cost of belts bought
    System.out.printf ("\n The total sales tax on pants is $" + "%.2f" , salestaxPants); // printing out the sales tax amount to be paid on pants
    System.out.printf ("\n The total sales tax on shirts is $" + "%.2f" , salestaxShirts); // printing out the sales tax amount to be paid on shirts
    System.out.printf ("\n The total sales tax on belts is $" + "%.2f" , salestaxBelts); // printing out the sales tax amount to be paid on belts
    System.out.printf ("\n The total cost of items before sales tax is $" + "%.2f" , totalcostItemsOnly); // printing out the total cost of items only without sales tax 
    System.out.printf ("\n The total sales tax paid for buying all of the items is $" + "%.2f" , totalSalesTax); // printing out the total sales tax to be paid
    System.out.printf ("\n The grand total of items bought including sales tax is $" + "%.2f" , totalcostWithTax); // printing out the grand total of all the items bought including sales tax
    System.out.printf ("\n"); // moving the instruction to a new line
  
  } 
} //terminating the code