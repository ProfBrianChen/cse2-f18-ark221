/* 
Name: Arzoo Karki
CSE 002-310
Professor Arielle Karr
09/25/2018
Program to use 'Switch' statement to evaluate blocks of statements and produce the required output
*/

import java.util.Scanner; // importing the Scanner class to use it

public class CrapsSwitch { // declaring class as CrapsSwitch
  public static void main (String args []) { //initializing main method
    Scanner myOwnScanner = new Scanner (System.in); // declaring a scanner object
    
    // in the following lines, we are declaring a bunch of string statements and assigning
    // them with their respective outcomes for the game based on the given question 
    
    String oneAndone = "Snake Eye";
    String oneAndtwo = "Ace Deuce";
    String oneAndthree = "Easy Four";
    String oneAndfour = "Fever Five";
    String oneAndfive = "Easy Six";
    String oneAndSix = "Seven out";
    String twoAndtwo = "Hard four";
    String twoAndthree = "Fever five";
    String twoAndfour = "Easy six";
    String twoAndfive = "Seven out";
    String twoAndsix = "Easy Eight";
    String threeAndthree = "Hard six";
    String threeAndfour = "Seven out";
    String threeAndfive = "Easy Eight";
    String threeAndsix = "Nine"; 
    String fourAndfour = "Hard Eight";
    String fourAndfive = "Nine"; 
    String fourAndsix = "Easy Ten";
    String fiveAndfive = "Hard Ten";
    String fiveAndsix = "Yo-leven"; 
    String sixAndsix = "Boxcars";
     
    // generating two random numbers to be evaluated
    int randNum1 = (int) (Math.random() * (5+1)) + 1;
    int randNum2 = (int) (Math.random() * (5+1)) + 1;
    
    System.out.print("Enter 1 if you would like to randomly cast a die. Enter 2 if you would like to state the two die to be evaluated."); // prompting the user to select one of the two options
    
    int userInput = myOwnScanner.nextInt(); // declaring a statement to accept the user's input
    
    // here, we are declaring variables to store values input by the user
    int inputValue1, inputValue2;
    
    switch (userInput) { // checking if the user wants random numbers or to input numbers
      case 1: // declaring execution method if user wants two randomly generated numbers
        switch (randNum1) { // evaluating the first number
            
            // here on, we are evaluating the first and the second random number 
            // and producing the respective terminology
          case 1:
            switch (randNum2) {
              case 1: System.out.println("Your slang terminology is: " + oneAndone );
                break;
              case 2: System.out.println("Your slang terminology is: " + oneAndtwo );
                break;
              case 3: System.out.println("Your slang terminology is: " + oneAndthree );
                break;
              case 4: System.out.println("Your slang terminology is: " + oneAndfour );
                break;
              case 5: System.out.println("Your slang terminology is: " + oneAndfive );
                break;
              case 6: System.out.println("Your slang terminology is: " + oneAndSix );
                break;
            }
            break;
          case 2:
            switch (randNum2) {
              case 1: System.out.println("Your slang terminology is: " + oneAndtwo );
                break;
              case 2: System.out.println("Your slang terminology is: " + twoAndtwo );
                break;
              case 3: System.out.println("Your slang terminology is: " + twoAndthree );
                break;
              case 4: System.out.println("Your slang terminology is: " + twoAndfour );
                break;
              case 5: System.out.println("Your slang terminology is: " + twoAndfive );
                break;
              case 6: System.out.println("Your slang terminology is: " + twoAndsix );
                break;
            }
            break;
          case 3:
            switch (randNum2) {
              case 1: System.out.println("Your slang terminology is: " + oneAndthree );
                break;
              case 2: System.out.println("Your slang terminology is: " + twoAndthree );
                break;
              case 3: System.out.println("Your slang terminology is: " + threeAndthree );
                break;
              case 4: System.out.println("Your slang terminology is: " + threeAndfour );
                break;
              case 5: System.out.println("Your slang terminology is: " + threeAndfive );
                break;
              case 6: System.out.println("Your slang terminology is: " + threeAndsix );
                break;
            }
            break;
          case 4:
            switch (randNum2) {
              case 1: System.out.println("Your slang terminology is: " + oneAndfour );
                break;
              case 2: System.out.println("Your slang terminology is: " + twoAndfour );
                break;
              case 3: System.out.println("Your slang terminology is: " + threeAndfour );
                break;
              case 4: System.out.println("Your slang terminology is: " + fourAndfour );
                break;
              case 5: System.out.println("Your slang terminology is: " + fourAndfive );
                break;
              case 6: System.out.println("Your slang terminology is: " + fourAndsix );
                break;
            }
            break;
          case 5:
            switch (randNum2) {
              case 1: System.out.println("Your slang terminology is: " + oneAndfive );
                break;
              case 2: System.out.println("Your slang terminology is: " + twoAndfive );
                break;
              case 3: System.out.println("Your slang terminology is: " + threeAndfive );
                break;
              case 4: System.out.println("Your slang terminology is: " + fourAndfive );
                break;
              case 5: System.out.println("Your slang terminology is: " + fiveAndfive );
                break;
              case 6: System.out.println("Your slang terminology is: " + fiveAndsix );
                break;
            }
            break;
          case 6:
            switch (randNum2) {
              case 1: System.out.println("Your slang terminology is: " + oneAndSix );
                break;
              case 2: System.out.println("Your slang terminology is: " + twoAndsix );
                break;
              case 3: System.out.println("Your slang terminology is: " + threeAndsix );
                break;
              case 4: System.out.println("Your slang terminology is: " + fourAndsix );
                break;
              case 5: System.out.println("Your slang terminology is: " + fiveAndsix );
                break;
              case 6: System.out.println("Your slang terminology is: " + sixAndsix );
                break;
            }
            break;
        }
        break;
       
      case 2: // declaring execution method if user wants to input the two values to be evaluated
     
        System.out.print("Enter the first value to be evaluated. The valid range is between 1 and 6, including the end values: "); //prompting user to input the first value
        inputValue1 = myOwnScanner.nextInt(); // getting the first input from user
        
        System.out.print("Enter the second value to be evaluated. The valid range is between 1 and 6, including the end values: "); //prompting user to input the second value
        inputValue2 = myOwnScanner.nextInt(); // getting the second input from user
        
        switch (inputValue1) { // checking the first value input by the user
        
            // here on, we are evaluating the two numbers input by the user to produce the respective terminology
          case 1:
            switch (inputValue2) {
              case 1: System.out.println("Your slang terminology is: " + oneAndone );
                break;
              case 2: System.out.println("Your slang terminology is: " + oneAndtwo );
                break;
              case 3: System.out.println("Your slang terminology is: " + oneAndthree );
                break;
              case 4: System.out.println("Your slang terminology is: " + oneAndfour );
                break;
              case 5: System.out.println("Your slang terminology is: " + oneAndfive );
                break;
              case 6: System.out.println("Your slang terminology is: " + oneAndSix );
                break;
              default:
                System.out.println("Please enter your inputs within a valid range."); // prompting the user to input values within range only
                break;
            }
            break;
          case 2:
            switch (inputValue2) {
              case 1: System.out.println("Your slang terminology is: " + oneAndtwo );
                break;
              case 2: System.out.println("Your slang terminology is: " + twoAndtwo );
                break;
              case 3: System.out.println("Your slang terminology is: " + twoAndthree );
                break;
              case 4: System.out.println("Your slang terminology is: " + twoAndfour );
                break;
              case 5: System.out.println("Your slang terminology is: " + twoAndfive );
                break;
              case 6: System.out.println("Your slang terminology is: " + twoAndsix );
                break;
              default:
                System.out.println("Please enter your inputs within a valid range."); // prompting the user to input values within range only
                break;
            }
            break;
          case 3:
            switch (inputValue2) {
              case 1: System.out.println("Your slang terminology is: " + oneAndthree );
                break;
              case 2: System.out.println("Your slang terminology is: " + twoAndthree );
                break;
              case 3: System.out.println("Your slang terminology is: " + threeAndthree );
                break;
              case 4: System.out.println("Your slang terminology is: " + threeAndfour );
                break;
              case 5: System.out.println("Your slang terminology is: " + threeAndfive );
                break;
              case 6: System.out.println("Your slang terminology is: " + threeAndsix );
                break;
              default:
                System.out.println("Please enter your inputs within a valid range."); // prompting the user to input values within range only
                break;
            }
            break;
          case 4:
            switch (inputValue2) {
              case 1: System.out.println("Your slang terminology is: " + oneAndfour );
                break;
              case 2: System.out.println("Your slang terminology is: " + twoAndfour );
                break;
              case 3: System.out.println("Your slang terminology is: " + threeAndfour );
                break;
              case 4: System.out.println("Your slang terminology is: " + fourAndfour );
                break;
              case 5: System.out.println("Your slang terminology is: " + fourAndfive );
                break;
              case 6: System.out.println("Your slang terminology is: " + fourAndsix );
                break;
              default:
                System.out.println("Please enter your inputs within a valid range."); // prompting the user to input values within range only
                break;
            }
            break;
          case 5:
            switch (inputValue2) {
              case 1: System.out.println("Your slang terminology is: " + oneAndfive );
                break;
              case 2: System.out.println("Your slang terminology is: " + twoAndfive );
                break;
              case 3: System.out.println("Your slang terminology is: " + threeAndfive );
                break;
              case 4: System.out.println("Your slang terminology is: " + fourAndfive );
                break;
              case 5: System.out.println("Your slang terminology is: " + fiveAndfive );
                break;
              case 6: System.out.println("Your slang terminology is: " + fiveAndsix );
                break;
              default:
                System.out.println("Please enter your inputs within a valid range."); // prompting the user to input values within range only
                break;
            }
            break;
          case 6:
            switch (inputValue2) {
              case 1: System.out.println("Your slang terminology is: " + oneAndSix );
                break;
              case 2: System.out.println("Your slang terminology is: " + twoAndsix );
                break;
              case 3: System.out.println("Your slang terminology is: " + threeAndsix );
                break;
              case 4: System.out.println("Your slang terminology is: " + fourAndsix );
                break;
              case 5: System.out.println("Your slang terminology is: " + fiveAndsix );
                break;
              case 6: System.out.println("Your slang terminology is: " + sixAndsix );
                break;
              default:
                System.out.println("Please enter your inputs within a valid range."); // prompting the user to input values within range only
                break;
            }
            break;
          default:
            System.out.println("Please enter your inputs within a valid range."); // prompting the user to input values within range only
            break;
        }
        break;
      default: 
        System.out.println("Please enter your inputs within a valid range."); // prompting the user to input values within range only
        break;
    }
  }
}
