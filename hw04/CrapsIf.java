/* 
Name: Arzoo Karki
CSE 002-310
Professor Arielle Karr
09/25/2018
Program to use 'if' statement to evaluate blocks of statements and produce the required output
*/

import java.util.Scanner; // importing the Scanner class to use it

public class CrapsIf { // declaring class as CrapsIf
  public static void main(String args[]){ //initializing main method
    
Scanner myOwnScanner = new Scanner (System.in); // declaring a scanner object
String Result = ""; //declaring a string variable to store result

System.out.print ("Enter 1 if you would like to randomly cast a die. Enter 2 if you would like to state the two die to be evaluated."); // prompting the user to select one of the two options

int userInput = myOwnScanner.nextInt(); // declaring a statement to accept the user's input
    
    if (userInput == 1) { // declaring execution method if user wants two randomly generated numbers
    // generating random numbers to be evaluated
    int randNum1 = (int) (Math.random () *( 5 + 1 )) + 1; 
    int randNum2 = (int) (Math.random () *( 5 + 1 )) + 1;
  
     // in all of the following lines, we are checking each of the two random numbers generated in order to output the respective slang terminology
  if (randNum1 == 1 && randNum2 == 1){
	Result = "Snake Eyes";
	}
	else if ((randNum1 == 1 && randNum2 == 2) || (randNum1 == 2 && randNum2 == 1)){
	Result = "Ace Deuce";
	}
	else if ((randNum1 == 1 && randNum2 == 3) || (randNum1 == 3 && randNum2 == 1)){
	Result = "Easy Four";
	}
	else if ((randNum1 == 1 && randNum2 == 4) || (randNum1 == 4 && randNum2 == 1)){
	Result = "Fever Five";
	}
	else if ((randNum1 == 1 && randNum2 == 5) || (randNum1 == 5 && randNum2 == 1)){
	Result = "Easy Six";
	}
	else if ((randNum1 == 1 && randNum2 == 6) || (randNum1 == 6 && randNum2 == 1)){
	Result = "Seven Out";
	}
	else if (randNum1 == 2 && randNum2 == 2){
	Result = "Hard Four";
	}
	else if ((randNum1 == 2 && randNum2 == 3) || (randNum1 == 3 && randNum2 == 2)){
Result = "Fever Five";
  }
    else if ((randNum1 == 1 && randNum2 == 5) || (randNum1 == 5 && randNum2 == 1)){
	Result = "Easy Six";
  }
	else if ((randNum1 == 1 && randNum2 == 6) || (randNum1 == 6 && randNum2 == 1)){
	Result = "Seven Out";
	}
	else if (randNum1 == 2 && randNum2 == 2){
	Result = "Hard Four";
	}
	else if ((randNum1 == 2 && randNum2 == 3) || (randNum1 == 3 && randNum2 == 2)){
	Result = "Fever Five";
	}
	else if ((randNum1 == 2 && randNum2 == 4) || (randNum1 == 4 && randNum2 == 2)){
	Result = "Easy Six";
	}
	else if ((randNum1 == 2 && randNum2 == 5) || (randNum1 == 5 && randNum2 == 2)){
	Result = "Seven Out";
	}
	else if ((randNum1 == 2 && randNum2 == 6) || (randNum1 == 6 && randNum2 == 2)){
	Result = "Easy Eight";
  }
	else if(randNum1 == 3 && randNum2 == 3){
	Result = "Hard Six";
	}
	else if ((randNum1 == 3 && randNum2 == 4) || (randNum1 == 4 && randNum2 == 3)){
	Result = "Seven Out";
  }
	else if ((randNum1 == 3 && randNum2 == 5) || (randNum1 == 5 && randNum2 == 3)){
	Result = "Easy Eight";
	}
	else if ((randNum1 == 3 && randNum2 == 6) || (randNum1 == 6 && randNum2 == 3)){
	Result = "Nine";
	}
	else if (randNum1 == 4 && randNum2 == 4){
	Result = "Hard Eight";
	}
	else if ((randNum1 == 4 && randNum2 == 5) || (randNum1 == 5 && randNum2 == 4)){
	Result = "Nine";
	}
	else if ((randNum1 == 4 && randNum2 == 6) || (randNum1 == 6 && randNum2 == 4)){
	Result = "Easy Ten";
	}
	else if (randNum1 == 5 && randNum2 == 5){
	Result = "Hard Ten";
	}
	else if ((randNum1 == 5 && randNum2 == 6) || (randNum1 == 6 && randNum2 == 5)){
	Result = "Yo-leven";
	}
	else if (randNum1 == 6 && randNum2 == 6){
	Result = "Boxcars";
	}
  System.out.println ("The slang terminology is: " + Result);
  }
        
  if (userInput == 2) { // declaring execution method if user wants to input the two values to be evaluated
	
    System.out.print ("Enter the first value to be evaluated. The valid range is between 1 and 6, including the end values: "); // prompting user to input the first value
	  int randNum1 = myOwnScanner.nextInt(); // getting the first input from user
	
    System.out.print ("Enter the second value to be evaluated. The valid range is between 1 and 6, including the end values: "); //prompting user to input the second value
	  int randNum2 = myOwnScanner.nextInt(); // getting the second input from user
	
  // first, we are checking if the values provided by the user are within range
  if ((randNum1 >= 1 && randNum1 <= 6) && (randNum2 >= 1 && randNum2 <= 6)) {
  // just like the first execution method, we are checking the numbers and producing the respective slang terminology
	if(randNum1 == 1 && randNum2 == 1) {
	Result = "Snake Eyes";
	}
	else if ((randNum1 == 1 && randNum2 == 2) || (randNum1 == 2 && randNum2 == 1)){
	Result = "Ace Deuce";
  }
	else if ((randNum1 == 1 && randNum2 == 3) || (randNum1 == 3 && randNum2 == 1)){
	Result = "Easy Four";
	}
	else if ((randNum1 == 1 && randNum2 == 4) || (randNum1 == 4 && randNum2 == 1)){
	Result = "Fever Five";
	}
	else if ((randNum1 == 1 && randNum2 == 5) || (randNum1 == 5 && randNum2 == 1)){
	Result = "Easy Six";
	}
	else if ((randNum1 == 1 && randNum2 == 6) || (randNum1 == 6 && randNum2 == 1)){
	Result = "Seven Out";
	}
	else if (randNum1 == 2 && randNum2 == 2){
	Result = "Hard Four";
	}
	else if ((randNum1 == 2 && randNum2 == 3) || (randNum1 == 3 && randNum2 == 2)){
	Result = "Fever Five";
  }
	else if ((randNum1 == 2 && randNum2 == 4) || (randNum1 == 4 && randNum2 == 2)){
	Result = "Easy Six";
  }
	else if ((randNum1 == 2 && randNum2 == 5) || (randNum1 == 5 && randNum2 == 2)){
	Result = "Seven Out";
  }
	else if ((randNum1 == 2 && randNum2 == 6) || (randNum1 == 6 && randNum2 == 2)){
	Result = "Easy Eight";
	}
	else if(randNum1 == 3 && randNum2 == 3){
	Result = "Hard Six";
	}
	else if ((randNum1 == 3 && randNum2 == 4) || (randNum1 == 4 && randNum2 == 3)){
	Result = "Seven Out";
	}
	else if ((randNum1 == 3 && randNum2 == 5) || (randNum1 == 5 && randNum2 == 3)){
	Result = "Easy Eight";
  }
	else if ((randNum1 == 3 && randNum2 == 6) || (randNum1 == 6 && randNum2 == 3)){
	Result = "Nine";
	}
	else if (randNum1 == 4 && randNum2 == 4){
	Result = "Hard Eight";
	}
	else if ((randNum1 == 4 && randNum2 == 5) || (randNum1 == 5 && randNum2 == 4)){
	Result = "Nine";
	}
	else if ((randNum1 == 4 && randNum2 == 6) || (randNum1 == 6 && randNum2 == 4)){
	Result = "Easy Ten";
  }
	else if (randNum1 == 5 && randNum2 == 5){
	Result = "Hard Ten";
	}
	else if ((randNum1 == 5 && randNum2 == 6) || (randNum1 == 6 && randNum2 == 5)){
	Result = "Yo-leven";
	}
	else if (randNum1 == 6 && randNum2 == 6){
	Result = "Boxcars";
	}
  
    System.out.println ("The slang terminology is: " + Result); // printing out the desired output
  }
  else {
	
    System.out.println ("Please enter your inputs within a valid range."); // prompting the user to provide another set of values if the values were not valid
}	
  }
  }
}
