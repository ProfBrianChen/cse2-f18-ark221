/* 
Name: Arzoo Karki
CSE 002-310
Professor Arielle Karr
10/8/2018
Program to calculate winning in a game of poker using a 'while' loop 
*/

import java.util.Scanner; // importing the myOwnScanner class to use it

public class Hw05 { // declaring class as Hw05
  public static void main(String args[]){ //initializing main method
    
//initializing variables to be used througout the program  
int randNum1, randNum2, randNum3, randNum4, randNum5;
int lim = 0; // limiting the number of hands generated
int numPairs = 0;
int count4ofAKind = 0; // four-of-a-kind
int count3ofAKind = 0; // three-of-a-kind
int countTwoPairs = 0; // two pairs
int countOnePair = 0; //one pair
int numHands = 0;
String inValid;
    
Scanner myOwnScanner = new Scanner (System.in); // declaring a myOwnScanner object
System.out.print ("Enter the times you want to generate a hand: "); //prompting the user for an input
    
while (!myOwnScanner.hasNextInt()) {
System.out.print ("Your input is invalid. Enter the integer number of times: ");
inValid = myOwnScanner.next();
        }
numHands = myOwnScanner.nextInt();
    
 while (lim < numHands){ // ensuring user doesn't input 0 for number of hands to be generated
     
   //generating random numbers
   randNum1 = (int) (Math.random() * (52)) + 1;
            
   do {
   randNum2 = (int) (Math.random() * (52)) + 1;
            }
   while (randNum2==randNum1); // in these lines, we use a do-while loop and check if the generated numbers are equal to each other
                          // in that case, we generate different numbers
   do{
   randNum3 = (int) (Math.random() * (52)) + 1;
            }
   while (randNum3==randNum2 || randNum3==randNum1);
   
   do {
   randNum4 = (int) (Math.random() * (52)) + 1;
            }
   while (randNum4==randNum3 || randNum4 == randNum2 || randNum4 == randNum1);
   
   do {
   randNum5 = (int) (Math.random() * (52)) + 1;
            }
   while (randNum5==randNum4 || randNum5==randNum3 || randNum5 == randNum2 || randNum5 == randNum1);            
            
   int modrandNum1 = randNum1 % 13;
   int modrandNum2 = randNum2 % 13;
   int modrandNum3 = randNum3 % 13;
   int modrandNum4 = randNum4 % 13;
   int modrandNum5 = randNum5 % 13;
                      
   // in the following lines, we are looking of pairs in the generated hand
            if (modrandNum1 == modrandNum2){
                numPairs++;
            }
            if (modrandNum1 == modrandNum3){
                numPairs++;
            }
            if (modrandNum1 == modrandNum4){
                numPairs++;
            }
            if (modrandNum1 == modrandNum5){
                numPairs++;
            }
            if (modrandNum2 == modrandNum3){
                numPairs++;
            }
            if (modrandNum2 == modrandNum4){
                numPairs++;
            }
            if (modrandNum2 == modrandNum5){
                numPairs++;
            }
            if (modrandNum3 == modrandNum4){
                numPairs++;
            }
            if (modrandNum3 == modrandNum5){
                numPairs++;
            }
            if (modrandNum4 == modrandNum5){
                numPairs++;
            }
            
   // now, we are looking at the number of pairs we got and checking if it's a pair or two
     
            if (numPairs == 1){ 
                countOnePair++;
            }
            if (numPairs == 2 || numPairs ==4){
                countTwoPairs++;
            }
            if (numPairs == 3 || numPairs ==4){
                count3ofAKind++;
            }
            if (numPairs == 6){
                count4ofAKind++;
            }
            
            lim++;
        }
        
    //here, we are calculating required probabilties by dividing the probabilty of that particular set of cards
    // divided by the total possible set of cards
        double Prob4 = (double)count4ofAKind / (double)numHands;
        double Prob3 = (double)count3ofAKind / (double)numHands;
        double ProbTwoPairs = (double)countTwoPairs / (double)numHands;
        double prob1Pair = (double)countOnePair / (double)numHands;
        
        System.out.println("The number of hands generated is: " + numHands);
        System.out.printf("Probability of getting four of a kind: %1.3f \n", Prob4); // editing the output to 3 decimal places
        System.out.printf("Probability of getting three of a kind: %1.3f \n", Prob3);
        System.out.printf("Probability of getting two pairs: %1.3f \n", ProbTwoPairs);
        System.out.printf("Probability of getting a pair: %1.3f \n", prob1Pair);
    }  
}
        
    