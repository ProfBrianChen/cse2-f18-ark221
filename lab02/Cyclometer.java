public class Cyclometer { // opening of every Java program
   public static void main (String [] args) {
    int secsTrip1 = 580;  // assigning a value to the interger variable secsTrip1
    int secsTrip2 = 322;  // assigning a value to the interger variable secsTrip2
		int countsTrip1 = 1300;  // assigning a value to the interger variable countsTrip1
		int countsTrip2 = 2500; // assigning a value to the interger variable countsTrip2
          
    double wheelDiameter = 27.0;  //declaring constants we will need throughout this operation in the type that suits best 
  	double pi = 3.14159; // declaring constants we will need throughout this operation in the type that suits best
  	int feetPerMile = 5280;  // declaring constants we will need throughout this operation in the type that suits best
  	int inchesPerFoot = 12;   // declaring constants we will need throughout this operation in the type that suits best
  	int secondsPerMinute = 60;  // declaring constants we will need throughout this operation in the type that suits best
	  double distanceTrip1, distanceTrip2,totalDistance;  // assigning the three variables to hold decimals instead of just integers
     
     System.out.println ("Trip 1 took " + (double) (secsTrip1 / secondsPerMinute ) +" minutes and had " + countsTrip1+" counts."); //casting the number of minutes from integer to double and printing out the time taken by each trip and the number of counts for each trip
	   System.out.println ("Trip 2 took " + (double )(secsTrip2 / secondsPerMinute) +" minutes and had " + countsTrip2+" counts."); //casting the number of minutes from integer to double and printing out the time taken by each trip and the number of counts for each trip

     distanceTrip1 = countsTrip1 * wheelDiameter * pi; // calculating distance in inches, 
     distanceTrip1 = inchesPerFoot * feetPerMile; // calculating distance in miles
	   distanceTrip2 = countsTrip2 * wheelDiameter * pi; // calculating distance in inches again
	   totalDistance = distanceTrip1 + distanceTrip2; //calculating total distance by adding up the two distances
     
     System.out.println ("Trip 1 was "+distanceTrip1+" miles"); // printing out the distance for the first trip
	   System.out.println ("Trip 2 was "+distanceTrip2+" miles"); // printing out the distance for the second trip
	   System.out.println ("The total distance was "+totalDistance+" miles"); // printing out the total distance of two trips
        
   } // end of main method
}